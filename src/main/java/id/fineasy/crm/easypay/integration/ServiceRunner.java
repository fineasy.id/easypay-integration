package id.fineasy.crm.easypay.integration;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class ServiceRunner implements ApplicationRunner {

    @Autowired
    private ApplicationContext context;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("Service runner started!");
        log.info("Current datetime: "+DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}
