package id.fineasy.crm.easypay.integration.worker;

import id.fineasy.crm.easypay.integration.component.ConfigUtils;
import id.fineasy.crm.easypay.integration.component.DataUtils;
import id.fineasy.crm.easypay.integration.component.Mailer;
import id.fineasy.crm.easypay.integration.component.MessageSender;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.text.StrSubstitutor;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.File;
import java.io.FileWriter;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.easypay.integration.component.DataUtils.*;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.*;

@Component
public class CsvDeduction implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    Mailer mailer;
    @Autowired
    ConfigUtils configUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    final DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");


    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("CSV deduction generator started!");
    }

    //@Scheduled(cron = "1 0 10 * * *")
    public void test() {
        log.info("Crontab running ....");
        messageSender.send(QUEUE_CSV_DEDUCTION_GENERATOR, new JSONObject());
    }

    @JmsListener(destination = QUEUE_CSV_DEDUCTION_GENERATOR)
    public void csvGenerator(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Received request to generate deduction");
        asyncExecutor.execute(new CsvDeduction.CsvDeductionWorker());
    }

    private class CsvDeductionWorker implements Runnable {
        boolean sendEmail = false;

        @Override
        public void run() {
            log.info("Start generating CSV ...");

            try {
                File csvFile = generateCsv();
                sendCsv(csvFile);
            } catch (Exception e) {
                log.error("Error generated and send CSV file! "+e,e);
            }
        }

        private void sendCsv(File csvFile) throws Exception {
            if (!sendEmail) {
                log.info("No need to send email, deduction list is empty!");
                return;
            }
            MapSqlParameterSource params = new MapSqlParameterSource();
            String sql = " SELECT cfg_val FROM config WHERE cfg_key like 'csv.request.to%' ";
            List<String> recipients = dataUtils.getJT().queryForList(sql, params, String.class);
            String subject = "[DEDUCTION] Finstar deduction request "+DateTime.now().toString("dd.MM.yyyy");
            String body = configUtils.getConfig("csv.deduction.email.body");

            Map<String,String> subTexts = new HashMap<>();
            subTexts.put("generated", DateTime.now().toString("dd/MM/yyyy HH:mm:ss"));
            StrSubstitutor substitutor = new StrSubstitutor(subTexts);
            body = substitutor.replace(body);

            List<File> attachments = new ArrayList<>();
            attachments.add(csvFile);

            mailer.send(recipients,subject,body,attachments);
        }

        private File generateCsv() throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("deductOn", DateTime.now().toString("yyyy-MM-dd"));

            String sql = " SELECT d.*, c.valid_seq, c.full_name, c.easypay_id FROM contract_deduction d JOIN _contract c ON (d.contract_id=c.contract_id) WHERE 1" +
                    " AND deduct_on=:deductOn AND c.is_active=1 AND d.payable>0 ";
            List<Map<String,Object>> rows = dataUtils.getJT().queryForList(sql, params);
            String[] csvHeaders = { "CODE", "TYPE", "RECORD", "ID", "NAME", "TRANSACTION", "AMOUNT", "DATE",	"TO" };
            File csvFile = new File("/tmp/finstar-deduction-request-"+DateTime.now().toString("yyyy-MM-dd_HH")+".csv");

            FileWriter fileWriter = new FileWriter(csvFile);
            CSVPrinter csvPrinter = new CSVPrinter(fileWriter, CSVFormat.EXCEL.withHeader(csvHeaders));

            String csvFormatDate = "dd/MM/yyyy";
            for (Map<String,Object> row:rows ) {
                DateTime date = DateTime.parse(""+row.get("deduct_on"), dtf);
                csvPrinter.printRecord("1", "Gen"+row.get("valid_seq"),""+row.get("contract_id"),row.get("easypay_id"),row.get("full_name"),"DEDUCT ONCE",row.get("payable"),date.toString(csvFormatDate),"N/A");
                sendEmail = true;
            }

            csvPrinter.flush();
            fileWriter.close();

            log.info("CSV file : "+csvFile.getAbsolutePath()+" is crreated!");


            return csvFile;
        }
    }
}
