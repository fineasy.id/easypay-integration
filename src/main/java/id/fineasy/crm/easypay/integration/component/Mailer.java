package id.fineasy.crm.easypay.integration.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

@Component
public class Mailer {
    @Autowired
    public JavaMailSender mailSender;

    @Autowired
    private DataUtils dataUtils;

    @Value("${spring.mail.from}")
    private String from;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void send(List<String> to, String subject, String htmlContent, List<File> attachments) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

        String[] toList = new String[to.size()];
        toList = to.toArray(toList);

        helper.setTo(toList);
        helper.setFrom(from);
        helper.setBcc(from);
        helper.setSubject(subject);
        helper.setText(htmlContent, true);

        if (attachments!=null) {
            for (File attachment : attachments ) {
                helper.addAttachment(attachment.getName(),attachment);
            }
        }

        boolean retry = false;
        int retryCount = 0;
        do {
            try {
                retry = false;
                mailSender.send(mailMessage);
                log.info("Email sent!");
            } catch (Exception e) {
                log.error("Error sending email: "+e,e);
                if (retryCount < 3) {
                    log.info("Retrying ...");
                    retry  = true;
                    retryCount++;
                }
            }
        } while(retry);

    }

    public void send(String to, String subject, String htmlContent, File attachment) throws Exception {
        ArrayList<String> tos = new ArrayList<>();
        tos.add(to);
        ArrayList<File> attachments = new ArrayList<>();
        attachments.add(attachment);
        send(tos, subject, htmlContent, attachments);
    }

}
