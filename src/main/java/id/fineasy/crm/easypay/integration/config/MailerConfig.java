package id.fineasy.crm.easypay.integration.config;

import id.fineasy.crm.easypay.integration.component.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import java.lang.invoke.MethodHandles;
import java.util.Properties;

@Configuration
public class MailerConfig {
    @Autowired
    ConfigUtils configUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${spring.mail.host}")
    String mailHost;
    @Value("${spring.mail.port}")
    int mailPort;
    @Value("${spring.mail.username}")
    String mailUsername;
    @Value("${spring.mail.password}")
    String mailPassword;

    @Bean(name="mailSender")
    @Primary
    public JavaMailSender getJavaMailSender() {
        log.info("Init mail host: "+mailHost+":"+mailPort+", username: "+mailUsername+" / "+mailPassword);
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailHost);
        mailSender.setPort(mailPort);
        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        mailSender.setJavaMailProperties(props);

        return mailSender;
    }
}
