package id.fineasy.crm.easypay.integration.worker;

import id.fineasy.crm.easypay.integration.component.ConfigUtils;
import id.fineasy.crm.easypay.integration.component.DataUtils;
import id.fineasy.crm.easypay.integration.component.Mailer;
import id.fineasy.crm.easypay.integration.component.MessageSender;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.text.StrSubstitutor;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import javax.jms.Session;
import java.io.File;
import java.io.FileWriter;
import java.lang.invoke.MethodHandles;
import java.util.*;

import static id.fineasy.crm.easypay.integration.component.DataUtils.*;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.*;

@Component
public class CsvDisbursement implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    Mailer mailer;
    @Autowired
    ConfigUtils configUtils;

    private static int counter = 0;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    final DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        log.info("CSV disbursement generator started!");
    }

    //@Scheduled(cron = "1 30 17 * * *")
    public void test() {
        counter+=1;
        log.info("Crontab running ...."+counter);
        messageSender.send(QUEUE_CSV_DISBURSEMENT_GENERATOR, new JSONObject());
    }

    @JmsListener(destination = QUEUE_CSV_DISBURSEMENT_GENERATOR)
    public void csvGenerator(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Received request to generate disbursement");
        asyncExecutor.execute(new CsvDisbursementWorker());
    }

    private class CsvDisbursementWorker implements Runnable {
        boolean sendEmail = false;

        @Override
        public void run() {
            log.info("Start generating CSV ...");

            try {
                File csvFile = generateCsv();
                sendCsv(csvFile);
            } catch (Exception e) {
                log.error("Error generated CSV file! "+e,e);
            }
        }

        private void sendCsv(File csvFile) throws Exception {
            if (!sendEmail) {
                log.info("No need to send email, disbursement list is empty!");
                return;
            }
            MapSqlParameterSource params = new MapSqlParameterSource();
            String sql = " SELECT cfg_val FROM config WHERE cfg_key like 'csv.request.to%' ";
            List<String> recipients = dataUtils.getJT().queryForList(sql, params, String.class);
            String subject = "[DISBURSEMENT] Finstar disbursement request "+DateTime.now().toString("dd.MM.yyyy");
            String body = configUtils.getConfig("csv.disbursement.email.body");

            Map<String,String> subTexts = new HashMap<>();
            subTexts.put("generated", DateTime.now().toString("dd/MM/yyyy HH:mm:ss"));
            StrSubstitutor substitutor = new StrSubstitutor(subTexts);
            body = substitutor.replace(body);

            List<File> attachments = new ArrayList<>();
            attachments.add(csvFile);

            mailer.send(recipients,subject,body,attachments);
        }

        private File generateCsv() throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("statusId", STATUS_CASHFLOW_PENDING);
            params.addValue("categoryId", CATEGORY_CASHFLOW_DISBURSEMENT);
            params.addValue("typeId", TYPE_CASHFLOW_OUTFLOW);
            params.addValue("contractStatusId", STATUS_CONTRACT_SIGNED);

            String sql = " SELECT f.*, c.easypay_id, c.full_name, c.contract_start_on, ADDDATE(c.contract_start_on, INTERVAL 1 DAY) as charged_on, c.contract_charged_on, c.contract_end_on, c.valid_seq FROM cashflow f JOIN _contract c ON (c.contract_id=f.contract_id) WHERE 1 " +
                    " AND f.type_id=:typeId AND f.category_id=:categoryId AND f.status_id=:statusId AND c.status_id=:contractStatusId  " +
                    " AND f.id NOT IN (SELECT cashflow_id FROM easypay_disbursement_log WHERE cashflow_id=f.id) ";

            List<Map<String,Object>> rows = dataUtils.getJT().queryForList(sql, params);

            String[] csvHeaders = { "CODE", "TYPE", "RECORD", "ID", "NAME", "TRANSACTION", "AMOUNT", "DATE",	"TO" };

            File csvFile = new File("/tmp/finstar-disbursement-request-"+DateTime.now().toString("yyyy-MM-dd")+".csv");

            FileWriter fileWriter = new FileWriter(csvFile);
            CSVPrinter csvPrinter = new CSVPrinter(fileWriter, CSVFormat.EXCEL.withHeader(csvHeaders));

            String csvFormatDate = "dd/MM/yyyy";
            for (Map<String,Object> row:rows ) {

                DateTime startDate = DateTime.parse(""+row.get("contract_start_on"), dtf);
                DateTime endDate = DateTime.parse(""+row.get("contract_end_on"), dtf);

                csvPrinter.printRecord("1", "Gen"+row.get("valid_seq"),""+row.get("contract_id"),row.get("easypay_id"),row.get("full_name"),"DISBURSE",row.get("amount"),startDate.toString(csvFormatDate),endDate.toString(csvFormatDate));

                params = new MapSqlParameterSource();
                params.addValue("contractId", row.get("contract_id"));
                params.addValue("cashflowId", row.get("id"));

                sql = " INSERT IGNORE INTO easypay_disbursement_log (contract_id,cashflow_id) VALUES (:contractId,:cashflowId) ";
                dataUtils.getJT().update(sql, params);
                sendEmail = true;
            }

            csvPrinter.flush();
            fileWriter.close();

            log.info("CSV file : "+csvFile.getAbsolutePath()+" is crreated!");

            return csvFile;
        }
    }
}
