package id.fineasy.crm.easypay.integration.controller;

import id.fineasy.crm.easypay.integration.component.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.lang.invoke.MethodHandles;

import static id.fineasy.crm.easypay.integration.component.EasypayUtils.METHOD_STATUS_INQUIRY;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.*;

@RestController
public class RestApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MessageSender messageSender;
    @Autowired
    ConfigUtils cfg;
    @Autowired
    DataUtils du;
    @Autowired
    HttpUtils httpUtil;
    @Autowired
    EasypayUtils easypayUtils;

    @RequestMapping(value = {"/csv/disbursement"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> csvDisbursement(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        messageSender.send(QUEUE_CSV_DISBURSEMENT_GENERATOR, jsonResult);

        return defaultReturn(jsonResult);
    }

    @RequestMapping(value = {"/csv/deduction"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> csvDeduction(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        messageSender.send(QUEUE_CSV_DEDUCTION_GENERATOR, jsonResult);

        return defaultReturn(jsonResult);
    }

    @RequestMapping(value = {"/disburse/{contractId}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> disburse(HttpServletRequest request, @PathVariable("contractId") int contractId) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        log.info("Disburse: "+contractId);
        jsonResult.put("contractId", contractId);
        messageSender.send(QUEUE_EASYPAY_DISBURSEMENT, jsonResult,3000L);

        return defaultReturn(jsonResult);
    }

    @RequestMapping(value = {"/deduct"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);
        messageSender.send(QUEUE_EASYPAY_DEDUCTION, jsonResult,3000L);
        return defaultReturn(jsonResult);
    }

    @RequestMapping(value = {"/status/{trxId}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> status(HttpServletRequest request, @PathVariable("trxId") String trxId) {
        JSONObject jsonResult = new JSONObject();

        String apiType = cfg.getConfig("easypay.api.type");

        try {
            JSONObject jsonToken = easypayUtils.getValidToken();

            final String secretKey = cfg.getConfig("easypay.api.secret.key." + apiType);
            String signatureRaw = trxId + secretKey;
            String signature = DigestUtils.sha256Hex(signatureRaw);
            final String trxTime = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");

            JSONObject jsonReq = new JSONObject();
            jsonReq.put("TRANSACTIONID", trxId);
            jsonReq.put("METHOD", METHOD_STATUS_INQUIRY);
            jsonReq.put("SIGNATURE", signature);

            String url = cfg.getConfig("easypay.api.trx.url." + apiType) + "/v1/credits/transaction/status";
            String authHeader = "Bearer " + jsonToken.getString("access_token");
            log.debug("Access URL: " + url + ", token: " + authHeader);
            JSONObject jsonResp = easypayUtils.postJson(url, jsonReq, authHeader);

            jsonResult.put("RESPONSE", jsonResp);

        } catch (Exception e) {
            jsonResult.put("msg", e.getMessage());
        }

        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(4), headers, httpStatus);
    }



    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

}
