package id.fineasy.crm.easypay.integration.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.converter.SimpleMessageConverter;


import javax.jms.ConnectionFactory;

@EnableJms
@Configuration
public class ActiveMQConfig {

    public static final String QUEUE_SMS = "queue-sms-sender";
    public static final String QUEUE_NEW_CONTRACT = "queue-new-contract";
    public static final String QUEUE_ACTIVATE_CONTRACT = "queue-activate-contract";
    public static final String QUEUE_RECALCULATE_CONTRACT = "queue-recalculate-contract";
    public static final String QUEUE_DEDUCTION_CONTRACT = "queue-deduction-contract";
    public static final String QUEUE_CSV_DISBURSEMENT_GENERATOR = "queue-csv-disbursement-generator";
    public static final String QUEUE_CSV_DEDUCTION_GENERATOR = "queue-csv-deduction-generator";

    public static final String QUEUE_EASYPAY_DISBURSEMENT = "queue-easypay-disbursement";
    public static final String QUEUE_EASYPAY_DEDUCTION = "queue-easypay-deduction";
    public static final String QUEUE_EMAIL = "email-queue";

    /**
    @Value("${my.activemq.broker-url}") String brokerUrl;
    @Value("${my.activemq.user}") String brokerUsername;
    @Value("${my.activemq.password}") String brokerPassword;
**/

}

