package id.fineasy.crm.easypay.integration.component;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.tika.mime.MimeType;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.invoke.MethodHandles;
import java.util.*;

@Component
public class EasypayUtils {
    @Autowired
    ConfigUtils cfg;
    @Autowired
    DataUtils du;
    @Autowired
    HttpUtils httpUtil;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static String apiType = null;

    public final static String METHOD_DISBURSEMENT = "DISBURSE";
    public final static String METHOD_STATUS_INQUIRY = "STATUS INQUIRY";
    public final static String METHOD_DEDUCT_ONCE = "DEDUCT ONCE";


    public long insertToLog(JSONObject contractDetail, JSONObject jsonReq) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.has("contract_id")?contractDetail.get("contract_id"):null);
        params.addValue("cashflowId", contractDetail.has("cashflow_id")?contractDetail.get("cashflow_id"):null);
        params.addValue("method", jsonReq.has("METHOD")?jsonReq.get("METHOD"):null);
        params.addValue("code", jsonReq.has("CODE")?jsonReq.get("CODE"):null);
        params.addValue("transactionTime", jsonReq.has("TRANSACTIONTIME")?jsonReq.get("TRANSACTIONTIME"):null);
        params.addValue("signature", jsonReq.has("SIGNATURE")?jsonReq.get("SIGNATURE"):null);
        JSONObject jsonTrx = jsonReq.getJSONArray("TRANSACTIONS").getJSONObject(0);
        params.addValue("trxId", jsonTrx.has("TRANSACTIONID")?jsonTrx.get("TRANSACTIONID"):null);
        params.addValue("easypayId", jsonTrx.has("ID")?jsonTrx.get("ID"):null);
        params.addValue("amount", jsonTrx.has("AMOUNT")?jsonTrx.get("AMOUNT"):null);

        String sql = " INSERT INTO easypay_api_log (contract_id,cashflow_id,method,code,transaction_time,signature,trx_id,easypay_id,amount) " +
                " VALUES (:contractId,:cashflowId,:method,:code,:transactionTime,:signature,:trxId,:easypayId,:amount) ";

        KeyHolder holder = new GeneratedKeyHolder();
        du.getJT().update(sql, params, holder);
        Long logId = holder.getKey().longValue();
        return logId;
    }

    public void updateLog(long logId, Object respReceiptId, Object respStatus, Object respValue, Object respError) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("logId", logId);
        params.addValue("respReceiptId", respReceiptId);
        params.addValue("respStatus", respStatus);
        params.addValue("respValue", respValue);
        params.addValue("respError", respError);

        String sql = " UPDATE easypay_api_log SET resp_receipt_id=:respReceiptId, resp_status=:respStatus, resp_value=:respValue, resp_error=:respError, in_process=0 " +
                " WHERE id=:logId ";
        du.getJT().update(sql, params);
    }

    public JSONObject getValidToken() throws Exception {
        MapSqlParameterSource params = du.newSqlParams();
        String sql = " SELECT * FROM easypay_api_token WHERE valid_until > ADDDATE(NOW(), INTERVAL 1 MINUTE) LIMIT 1 ";
        List<Map<String,Object>> rows = du.getJT().queryForList(sql, params);
        if (rows.size()<1) {
            boolean retryRequest = false;
            int retryCount = 0;
            do {
                try {
                    requestNewToken();
                } catch (Exception e) {
                    log.error("Error while requesting token: "+e,e);
                    retryCount++;
                    if (retryCount > 3) throw new Exception("Failed while getting token!");
                    try {
                        log.info("Retrying .... ");
                        Thread.sleep(3000);
                    } catch (Exception e2) {}
                    retryRequest = true;
                }
            } while(retryRequest);
            return getValidToken();
        }

        JSONObject validToken = new JSONObject(rows.get(0));
        return validToken;
    }

    private void requestNewToken() throws Exception {
        if (apiType == null) apiType = cfg.getConfig("easypay.api.type");
        String url = cfg.getConfig("easypay.api.token.url."+apiType);
        log.info("Token URL: "+url);

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("username", cfg.getConfig("easypay.api.token.username."+apiType));
        jsonReq.put("password", cfg.getConfig("easypay.api.token.password."+apiType));
        jsonReq.put("grant_type", "password");
        jsonReq.put("smsNumber", 1);

        JSONObject jsonResp = postJson(url, jsonReq);

        if (jsonResp.has("error") || !jsonResp.has("data")) {
            log.error("Error while requesting token!");
            throw new Exception("Unexpected token result!");
        }

        JSONObject jsonData = jsonResp.getJSONObject("data");

        MapSqlParameterSource params = du.newSqlParams();
        params.addValue("accessToken", jsonData.get("access_token"));
        params.addValue("expiresIn", jsonData.get("expires_in"));
        params.addValue("refreshToken", jsonData.get("refresh_token"));
        params.addValue("userId", jsonData.get("user_id"));
        params.addValue("timestamp", jsonResp.get("timestamp"));
        params.addValue("tokenType", jsonData.get("token_type"));

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(jsonData.getLong("expires_in"));
        DateTime dt = new DateTime(cal.getTime());
        log.info("Expire time "+ dt.toString("yyyy-MM-dd HH:mm:ss"));

        params.addValue("validUntil", dt.toString("yyyy-MM-dd HH:mm:ss"));

        String sql = " INSERT IGNORE INTO easypay_api_token (access_token,expires_in,refresh_token,user_id,token_type,timestamp,valid_until) VALUES " +
                " (:accessToken,:expiresIn,:refreshToken,:userId,:tokenType,:timestamp,:validUntil) ";

        du.getJT().update(sql, params);
    }

    public JSONObject inquiryStatus(Long logId) throws Exception {
        Long statusLogId = null;
        try {
            if (apiType == null) apiType = cfg.getConfig("easypay.api.type");
            log.info("Inquiry status #"+logId);
            String sql = " SELECT * FROM easypay_api_log WHERE id=:logId ";
            MapSqlParameterSource params = du.newSqlParams();
            params.addValue("logId", logId);

            Map<String,Object> row = du.getJT().queryForMap(sql, params);
            String trxId = ""+row.get("trx_id");

            final String secretKey = cfg.getConfig("easypay.api.secret.key." + apiType);
            String signatureRaw = trxId + secretKey;
            String signature = DigestUtils.sha256Hex(signatureRaw);
            final String trxTime = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");

            params.addValue("method", METHOD_STATUS_INQUIRY);
            params.addValue("signature", signature);
            params.addValue("trxTime", trxTime);

            KeyHolder holder = new GeneratedKeyHolder();
            sql = " INSERT INTO easypay_api_log (contract_id,cashflow_id,method,code,transaction_time,signature,trx_id,easypay_id,amount) " +
                    " SELECT contract_id,cashflow_id,:method,code,:trxTime,:signature,trx_id,easypay_id,amount FROM easypay_api_log WHERE id=:logId ";

            du.getJT().update(sql, params, holder);
            statusLogId = holder.getKey().longValue();
            log.debug("Status log id: "+statusLogId);

            JSONObject jsonToken = getValidToken();

            JSONObject jsonReq = new JSONObject();
            jsonReq.put("TRANSACTIONID", row.get("trx_id"));
            jsonReq.put("METHOD", METHOD_STATUS_INQUIRY);
            jsonReq.put("SIGNATURE", signature);

            String url = cfg.getConfig("easypay.api.trx.url." + apiType) + "/v1/credits/transaction/status";
            String authHeader = "Bearer " + jsonToken.getString("access_token");
            log.debug("Access URL: " + url + ", token: " + authHeader);
            JSONObject jsonResp = postJson(url, jsonReq, authHeader);

            String respReceiptId = (jsonResp.has("RECEIPTID") && !jsonResp.isNull("RECEIPTID")) ? "" + jsonResp.get("RECEIPTID") : null;
            String respStatus = (jsonResp.has("STATUS")) ? "" + jsonResp.get("STATUS") : null;
            Long respValue = (jsonResp.has("VALUE")&& !jsonResp.isNull("VALUE")) ? jsonResp.getLong("VALUE") : null;
            String respError = (jsonResp.has("ERROR_MESSAGE")) ? "" + jsonResp.get("ERROR_MESSAGE") : null;

            updateLog(statusLogId, respReceiptId, respStatus, respValue, respError);
            return jsonResp;

        } catch (Exception e) {
            if (statusLogId!=null)
                updateLog(statusLogId, null, "ERROR", null, e.getMessage());
            throw e;
        }
    }

    public JSONObject postJson(String url, JSONObject jsonObject, String authHeader) throws IOException {
        log.info("Post JSON to: "+url);
        if (authHeader==null) authHeader = cfg.getConfig("easypay.api.auth.header");

        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Authorization", authHeader);
        httpPost.addHeader("Content-type", "application/json");

        httpPost.setEntity(new StringEntity(jsonObject.toString(4)));
        CloseableHttpResponse response = httpUtil.getHttpClient().execute(httpPost);
        HttpEntity entity = response.getEntity();
        byte[] bytes = EntityUtils.toByteArray(entity);
        String respText = new String(bytes);
        log.info("Post JSON response: "+respText);

        JSONObject jsonResp = new JSONObject(respText);

        return jsonResp;
    }


    public JSONObject postJson(String url, JSONObject jsonObject) throws IOException {
        return postJson(url, jsonObject, null);
    }




    //public JSONObject getContract
}
