package id.fineasy.crm.easypay.integration.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class DbConfig {

    @Autowired
    DataSource dataSource;

    @Bean(name = "namedJdbcTemplate")
    public NamedParameterJdbcTemplate namedJdbcTemplate() {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Bean(name="trxManager")
    public DataSourceTransactionManager trxManager() {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name="trxTemplate")
    public TransactionTemplate trxTemplate() {
        return new TransactionTemplate(trxManager());
    }

}
