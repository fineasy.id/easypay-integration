package id.fineasy.crm.easypay.integration;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.TimeZone;

@EnableEurekaClient
@SpringBootApplication
public class Application {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {

        System.setProperty("server.servlet.context-path", "/api/easypay");
        ApplicationContext context =  SpringApplication.run(Application.class, args);

    }

    @PostConstruct
    public void init(){
        // Setting Spring Boot SetTimeZone
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
        log.info("Timezone was set! "+DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}
