package id.fineasy.crm.easypay.integration.component;

import org.apache.commons.io.FileUtils;
import org.apache.tika.utils.ExceptionUtils;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Utils {
    public static DecimalFormat getNumberFormat() {
        Locale locale = new Locale("id", "ID");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        String numberPattern = "#,##0";
        DecimalFormat decimalFormat = new DecimalFormat(numberPattern, symbols);
        return decimalFormat;
    }

    public static void sendError(ApplicationContext context, String subject, Exception e) {
        try {
            Mailer mailer = (Mailer) context.getBean("mailer");
            String err = ExceptionUtils.getStackTrace(e);
            File errFile = new File("/tmp/err-"+ DateTime.now().toString("yyyyMMdd_HHmmss")+".txt");
            FileUtils.write(errFile, err, Charset.forName("UTF-8"));
            String content = "<p><b>Exception: "+e+"</b></p><p style='font-family:\"Courier New\",Courier; font-size:8pt;'>"+err+"</p>";
            mailer.send("id.it@danafix.id", "[FINEASY ERROR!] - "+subject, content, errFile);
            errFile.delete();
        } catch (Exception e2) {}
    }
}
