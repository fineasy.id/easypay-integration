package id.fineasy.crm.easypay.integration.worker;

import id.fineasy.crm.easypay.integration.component.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandles;
import java.util.*;

import static id.fineasy.crm.easypay.integration.component.EasypayUtils.METHOD_DEDUCT_ONCE;
import static id.fineasy.crm.easypay.integration.component.EasypayUtils.METHOD_DISBURSEMENT;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.*;

@Component
public class EasypayDeduction implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    Mailer mailer;
    @Autowired
    ConfigUtils cfg;
    @Autowired
    EasypayUtils easypayUtils;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    Workbook wbReport;
    Sheet sheetReport;
    boolean sendEmail = false;
    int rowNum = 0;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }

    @Scheduled(cron = "1 0 10-23 * * *")
    public void scheduler() {
        messageSender.send(QUEUE_EASYPAY_DEDUCTION, new JSONObject());
    }

    @JmsListener(destination = QUEUE_EASYPAY_DEDUCTION)
    public void csvGenerator(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        JSONObject jsonReq = new JSONObject(jsonObjectString);
        log.info("Received request for deduction");
        try {
            processPayableContracts();

        } catch (Exception e) { log.error("Error run the worker: "+e,e); };
    }

    private void processPayableContracts() {
        sendEmail = false;
        wbReport = new XSSFWorkbook();
        sheetReport = wbReport.createSheet();
        rowNum = 0;
        sheetReport.setColumnWidth(0,100);

        Row headerRow = sheetReport.createRow(rowNum);
        headerRow.createCell(0).setCellValue("Date/Time");
        headerRow.createCell(1).setCellValue("Contract #");
        headerRow.createCell(2).setCellValue("Easypay ID");
        headerRow.createCell(3).setCellValue("Amount");
        headerRow.createCell(4).setCellValue("Status");
        headerRow.createCell(5).setCellValue("Deducted Amount");
        headerRow.createCell(6).setCellValue("Error Message");

        MapSqlParameterSource params = dataUtils.newSqlParams();
        String sql = " SELECT * FROM contract WHERE status_id IN (201,301) AND payable > 0 ";

        List<Map<String,Object>> rows = dataUtils.getJT().queryForList(sql, params);
        log.info("Found "+rows.size()+" contract!");

        for (Map<String,Object> row : rows) {
            try {
                log.info("Processing " + row);
                JSONObject jsonData = new JSONObject(row);
                EasypayDeductionWorker worker = new EasypayDeductionWorker(jsonData);
                worker.run();
            } catch (Exception e) {
                log.error("Error worker: "+e,e);
            }
        }

        final String rptFileName = "/tmp/fineasy-deduction-report-"+DateTime.now().toString("yyyy-MM-dd_HH")+".xlsx";
        File rptFile = new File(rptFileName);
        try {
            Calendar cal = Calendar.getInstance();
            int h = cal.get(Calendar.HOUR_OF_DAY);

            for (int i = 0; i <= 6; i++) {
                sheetReport.autoSizeColumn(i);
            }

            FileOutputStream outputStream = new FileOutputStream(rptFile);
            wbReport.write(outputStream);
            outputStream.close();
            wbReport.close();
            // send email
            List<File> attachments = new ArrayList<>();
            attachments.add(rptFile);
            List<String> to = new ArrayList<>();
            to.add("mon@fineasy.id");
            String mailBody = "Generated at : "+DateTime.now().toString("dd/MM/yyyy HH:mm:ss");
            if (h==10 || h==23 || sendEmail)
                mailer.send(to, "Deduction Report "+DateTime.now().toString("dd/MM/yyyy HH"), mailBody, attachments);
            else {
                log.info("Skipping sending email!");
            }
        } catch (Exception e) {
            log.error("error sending report: "+e);
        } finally {
            try {
                wbReport.close();
                FileUtils.forceDelete(rptFile);
            } catch (Exception e) {

            }
        }


        alertIfAnyFailedDeduction();
    }

    private void alertIfAnyFailedDeduction() {
        String[] mobiles = {"+6281282892453"};
        String sql = " SELECT COUNT(*) FROM easypay_api_deduct_error ";
        Integer found = dataUtils.getJT().queryForObject(sql, new MapSqlParameterSource(), Integer.class);
        log.info("Found failed deduction: "+found);
        if (found > 0) {
            String smsText = "ALERT!\nFound "+found+" failed deduction!\n"+DateTime.now().toString("dd/MM HH:mm:ss");
            for(String mobile: mobiles) {
                JSONObject jsonReq = new JSONObject();
                jsonReq.put("mobile", "+6281282892453");
                jsonReq.put("content", smsText);
                jsonReq.put("tag", "SYSTEM_ALERT");
                jsonReq.put("ref", UUID.randomUUID().toString());
                messageSender.send(QUEUE_SMS, jsonReq);
            }
        }
    }

    private class EasypayDeductionWorker {
        private final JSONObject jsonData;
        private final String apiType;
        private final JSONObject contractDetail;
        private final Long contractId;
        Long logId = null;
        private final String trxDate;
        private final String refId;
        private final Row rowRpt;

        public EasypayDeductionWorker(JSONObject jsonData) throws Exception {
            rowNum++;
            rowRpt = sheetReport.createRow(rowNum);
            apiType = cfg.getConfig("easypay.api.type");
            this.jsonData = jsonData;
            contractId = jsonData.getLong("contract_id");
            this.contractDetail = dataUtils.getContractDetail(contractId);
            trxDate = DateTime.now().toString("yyyy-MM-dd");
            refId = UUID.randomUUID().toString().toUpperCase();

            rowRpt.createCell(0).setCellValue(""+DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
            rowRpt.createCell(1).setCellValue(contractId);
        }

        public void run() {
            try {
                if (lastRequestWasError()) {
                    log.warn("Last request was error, please check table easypay_api_deduct_error!");
                    return;
                }
                this.deduct();
            } catch (Exception e) {
                log.error("Error deduction: "+e);
            }
        }

        private boolean lastRequestWasError() {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);

            String sql = " SELECT COUNT(*) FROM easypay_api_deduct_error WHERE contract_id=:contractId ";
            Integer found = dataUtils.getJT().queryForObject(sql, params, Integer.class);
            if (found>0) return true;
            return false;
        }

        private void deduct() throws Exception {

            try {
                final String secretKey = cfg.getConfig("easypay.api.secret.key." + apiType);
                final String trxTime = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");
                final String easypayCode = cfg.getConfig("easypay.api.code");

                JSONObject jsonReq = new JSONObject();
                jsonReq.put("METHOD", METHOD_DEDUCT_ONCE);
                jsonReq.put("CODE", easypayCode);
                jsonReq.put("TRANSACTIONTIME", trxTime);
                String signatureRaw = METHOD_DEDUCT_ONCE + easypayCode + trxTime + secretKey;

                log.debug("Signature (Raw): " + signatureRaw);
                String signature = DigestUtils.sha256Hex(signatureRaw);
                log.info("Sinature: " + signature);
                jsonReq.put("SIGNATURE", signature);

                String trxId = refId;
                JSONObject jsonTrx = new JSONObject();
                jsonTrx.put("TRANSACTIONID", trxId);
                jsonTrx.put("ID", contractDetail.getString("easypay_id"));

                rowRpt.createCell(2).setCellValue(contractDetail.getString("easypay_id"));

                jsonTrx.put("AMOUNT", jsonData.getLong("payable"));
                rowRpt.createCell(3).setCellValue(jsonData.getLong("payable"));

                JSONArray jsonTrxs = new JSONArray();
                jsonTrxs.put(jsonTrx);

                jsonReq.put("TRANSACTIONS", jsonTrxs);

                // add to log
                logId = easypayUtils.insertToLog(contractDetail, jsonReq);

                log.info("JSON Request: " + jsonReq.toString(4));

                sendRequest(jsonReq);
            } catch (Exception e) {
                log.error("Error while requesting deduction: "+e,e);
                if (logId !=null) {
                    easypayUtils.updateLog(logId, null, "ERROR", 0, ""+e);
                    addToError(e);
                    Utils.sendError(context,"Deduction error, contract #"+contractId+", trxID: "+refId, e);
                }
            }
        }

        private void addToError(Exception e) {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);
            params.addValue("logId", logId);
            params.addValue("error", ""+e);
            params.addValue("trxId", refId);

            String sql = " INSERT INTO easypay_api_deduct_error (contract_id,log_id,error,trx_id)" +
                    " VALUES (:contractId,:logId,:error,:trxId) ";

            dataUtils.getJT().update(sql, params);

        }

        private void sendRequest(JSONObject jsonReq) throws Exception {
            JSONObject jsonToken = easypayUtils.getValidToken();
            log.info("Valid token: " + jsonToken.toString());

            String url = cfg.getConfig("easypay.api.trx.url." + apiType) + "/v1/credits/transaction";

            String authHeader = "Bearer " + jsonToken.getString("access_token");
            log.debug("Access URL: " + url + ", token: " + authHeader);

            JSONObject jsonResp = easypayUtils.postJson(url, jsonReq, authHeader);
            log.info("Received response: " + jsonResp.toString(4));

            JSONObject jsonRespTrx = jsonResp.getJSONArray("TRANSACTIONS").getJSONObject(0);

            String respReceiptId = (jsonRespTrx.has("RECEIPTID") && !jsonRespTrx.isNull("RECEIPTID")) ? "" + jsonRespTrx.get("RECEIPTID") : null;
            String respStatus = (jsonRespTrx.has("STATUS")) ? "" + jsonRespTrx.get("STATUS") : null;
            Long respValue = (jsonRespTrx.has("VALUE")) ? jsonRespTrx.getLong("VALUE") : null;
            String respError = (jsonRespTrx.has("ERROR_MESSAGE")) ? "" + jsonRespTrx.get("ERROR_MESSAGE") : null;

            easypayUtils.updateLog(logId, respReceiptId, respStatus, respValue, respError);

            rowRpt.createCell(4).setCellValue((respStatus==null)?"UNKNOWN":respStatus);
            rowRpt.createCell(5).setCellValue((respValue==null)?0:respValue);
            rowRpt.createCell(6).setCellValue((respError==null)?"":respError);

            if (respStatus.equals("SUCCESS") || respStatus.equals("PARTIAL")) {
                sendEmail = true;
                updatePayment(respValue);
            } else {

            }
        }

        private void retry(Long logId) throws Exception {
            JSONObject jsonStatus = easypayUtils.inquiryStatus(logId);
        }

        private void updatePayment(Long respValue) throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);
            params.addValue("trxDate", trxDate);
            params.addValue("refId", refId);
            params.addValue("userId", 1);
            params.addValue("typeId", DataUtils.TYPE_CASHFLOW_INFLOW);
            params.addValue("categoryId", DataUtils.CATEGORY_CASHFLOW_REPAYMENT);
            params.addValue("statusId", DataUtils.STATUS_CASHFLOW_COMPLETED);
            params.addValue("amount", respValue);

            String sql = " INSERT INTO cashflow (ins_by,mod_by,contract_id,trx_date,ref_id,type_id,category_id,status_id,amount) " +
                    " VALUES (:userId,:userId,:contractId,:trxDate,:refId,:typeId,:categoryId,:statusId,:amount)  ";
            dataUtils.getJT().update(sql, params);

            log.debug("Recalculate contract #"+contractId);
            JSONObject jsonRecalculate = new JSONObject();
            jsonRecalculate.put("contractId", contractId);
            messageSender.send(QUEUE_RECALCULATE_CONTRACT, jsonRecalculate);
        }
    }

}
