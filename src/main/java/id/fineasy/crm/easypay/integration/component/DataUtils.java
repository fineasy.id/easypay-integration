package id.fineasy.crm.easypay.integration.component;

import org.apache.commons.text.WordUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class DataUtils {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    DataSourceTransactionManager trxManager;

    public final static int USER_ID_SYSTEM = 1;
    public static final int STATUS_NEW = 251;
    public static final int STATUS_REJECTED_MOBILE_EMPTY = 259;
    public static final int STATUS_GATE_UNEXPECTED_ERROR = 991;
    public static final int STATUS_GATE_PASSED_VERIFICATION = 252;
    public static final int STATUS_GATE_PASSED_TELESALES = 253;
    public static final int STATUS_GATE_REJECTED_ALLOWED_MINIMUM_REAPPLY_DAYS = 256;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_APPLICATION = 257;
    public static final int STATUS_GATE_REJECTED_DUPLICATE_ACTIVE_AGREEMENT = 258;
    public static final int STATUS_APPROVED = 101;

    public static final int STATUS_CONTRACT_SIGNED = 101;
    public static final int STATUS_CONTRACT_BEING_ACTIVATED = 121;
    public static final int STATUS_CONTRACT_ACTIVE = 201;
    public static final int STATUS_CONTRACT_OVERDUE = 301;
    public static final int STATUS_CONTRACT_CLOSED = 401;
    public static final int STATUS_CONTRACT_OVERPAYMENT = 402;
    public static final int STATUS_CONTRACT_CANCELED = 501;

    public static final int STATUS_CASHFLOW_PENDING = 2;
    public static final int STATUS_CASHFLOW_COMPLETED = 1;

    public static final int TYPE_CASHFLOW_INFLOW = 1;
    public static final int TYPE_CASHFLOW_OUTFLOW = 2;

    public static final int CATEGORY_CASHFLOW_DISBURSEMENT = 1;
    public static final int CATEGORY_CASHFLOW_REPAYMENT = 2;
    public static final int CATEGORY_CASHFLOW_REVERSAL_DISBURSEMENT = 3;
    public static final int CATEGORY_CASHFLOW_REVERSAL_REPAYMENT = 4;

    public static final int TELESALES_SOURCE_TYPE_INCOMPLETE_APPLICATION = 1;
    public static final int TELESALES_SOURCE_TYPE_ELIGIBLE_REPEAT = 2;
    public static final int TELESALES_SOURCE_TYPE_EXTERNAL_DATA = 3;

    public static final Map<Integer,String> daysBahasa;
    public static final Map<Integer,String> monthsBahasa;

    public static final int SUMMARY_PRINCIPAL = 1;
    public static final int SUMMARY_DAILY_DEDUCATION = 2;
    public static final int SUMMARY_TOTAL_DEBT = 3;
    public static final int SUMMARY_TOTAL_REPAYMENT = 4;
    public static final int SUMMARY_TOTAL_BALANCE = 5;
    public static final int SUMMARY_TODAY_PAYABLE = 6;
    public static final int SUMMARY_CONTRACT_ID = 7;
    public static final int SUMMARY_SIGNED_ON = 8;
    public static final int SUMMARY_ACTIVE_ON = 9;
    public static final int SUMMARY_DEDUCTION_STARTS_ON = 10;
    public static final int SUMMARY_DEDUCTION_ENDS_ON = 11;
    public static final int SUMMARY_DPD_CUR = 12;
    public static final int SUMMARY_DPD_MAX = 13;
    public static final int SUMMARY_TERM = 14;

    static {
        daysBahasa = new HashMap<>();
        daysBahasa.put(Calendar.SUNDAY, "Minggu");
        daysBahasa.put(Calendar.MONDAY, "Senin");
        daysBahasa.put(Calendar.TUESDAY, "Selasa");
        daysBahasa.put(Calendar.WEDNESDAY, "Rabu");
        daysBahasa.put(Calendar.THURSDAY, "Kamis");
        daysBahasa.put(Calendar.FRIDAY, "Jumat");
        daysBahasa.put(Calendar.SATURDAY, "Sabtu");


        monthsBahasa = new HashMap<>();
        monthsBahasa.put(0, "Januari");
        monthsBahasa.put(1, "Februari");
        monthsBahasa.put(2, "Maret");
        monthsBahasa.put(3, "April");
        monthsBahasa.put(4, "Mei");
        monthsBahasa.put(5, "Juni");
        monthsBahasa.put(6, "Juli");
        monthsBahasa.put(7, "Agustus");
        monthsBahasa.put(8, "September");
        monthsBahasa.put(9, "Oktober");
        monthsBahasa.put(10, "Nopember");
        monthsBahasa.put(11, "Desember");

    }

    public Map<String,Object> getTmpApplication(long tmpId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        String sql = " SELECT * FROM _application_gate WHERE tmp_id=:tmpId ";
        return namedJdbcTemplate.queryForMap(sql, params);
    }

    public JSONObject getContractDetail(long contractId) throws Exception {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);
            String sql = " SELECT * FROM _contract WHERE contract_id=:contractId ";

            Map<String,Object> row = namedJdbcTemplate.queryForMap(sql, params);
            return new JSONObject(row);
        } catch (Exception e) {
            throw new Exception("Failed to get contract #"+contractId);
        }
    }

    public Map<String,Object> contractToTextParams(JSONObject contractDetail) throws Exception {
        Map<String,Object> textParams = new HashMap<>();
        for (String key: contractDetail.keySet())  textParams.put(key,""+contractDetail.get(key));

        SimpleDateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat desDf = new SimpleDateFormat("dd/MM/yyyy");

        DecimalFormat decimalFormat = Utils.getNumberFormat();

        textParams.put("contract_start_on", desDf.format(srcDf.parse(""+textParams.get("contract_start_on"))));
        textParams.put("contract_charged_on", desDf.format(srcDf.parse(""+textParams.get("contract_charged_on"))));
        textParams.put("contract_end_on", desDf.format(srcDf.parse(""+textParams.get("contract_end_on"))));

        textParams.put("amount_approved", decimalFormat.format(contractDetail.get("amount_approved")));
        textParams.put("daily_charge", decimalFormat.format(contractDetail.get("daily_charge")));

        textParams.put("full_name", WordUtils.capitalizeFully(contractDetail.getString("full_name").toLowerCase()));

        Date signedOn = srcDf.parse(""+textParams.get("ins_on"));
        Calendar cal = Calendar.getInstance();
        cal.setTime(signedOn);

        int dow = cal.get(Calendar.DAY_OF_WEEK);
        textParams.put("signed_on_day", daysBahasa.get(dow));

        String m = monthsBahasa.get(cal.get(Calendar.MONTH));
        String dateBahasa = cal.get(Calendar.DATE)+" "+m+" "+cal.get(Calendar.YEAR);
        textParams.put("signed_on_date", dateBahasa);
        long totalAmount = contractDetail.getLong("daily_charge") * contractDetail.getInt("term_approved");
        textParams.put("amount_total", decimalFormat.format(totalAmount));

        return textParams;
    }

    public void updateTmpApplicationStatus(long tmpId, int statusId) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }

    public void updateTmpApplicationStatusAndProcessed(long tmpId, int statusId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("tmpId", tmpId);
        params.addValue("statusId", statusId);

        String sql = " UPDATE application_tmp SET status_id=:statusId, is_processed=1, processed_on=NOW() WHERE tmp_id=:tmpId ";
        namedJdbcTemplate.update(sql, params);
    }

    public MapSqlParameterSource newSqlParams() {
        return new MapSqlParameterSource();
    }

    public NamedParameterJdbcTemplate getJT() {
        return namedJdbcTemplate;
    }
    public DataSourceTransactionManager getTrx() {
        return trxManager;
    }


}
