package id.fineasy.crm.easypay.integration.worker;

import id.fineasy.crm.easypay.integration.component.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Session;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static id.fineasy.crm.easypay.integration.component.EasypayUtils.METHOD_DISBURSEMENT;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.QUEUE_CSV_DISBURSEMENT_GENERATOR;
import static id.fineasy.crm.easypay.integration.config.ActiveMQConfig.QUEUE_EASYPAY_DISBURSEMENT;

@Component
public class EasypayDisbursement implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    Mailer mailer;
    @Autowired
    ConfigUtils cfg;
    @Autowired
    EasypayUtils easypayUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_EASYPAY_DISBURSEMENT)
    public void csvGenerator(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {

        JSONObject jsonReq = new JSONObject(jsonObjectString);
        log.info("Received request for disbursement");
        Long contractId = jsonReq.getLong("contractId");
        try {
            EasypayDisbursementWorker worker = new EasypayDisbursementWorker(contractId);
            worker.run();
        } catch (Exception e) { log.error("Error run the worker: "+e,e); };
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

    }

    private void getAndDisburse() {

    }

    private class EasypayDisbursementWorker {
        private final Long contractId;
        private final Long cashflowId;
        private final JSONObject contractDetail;
        private final String apiType;
        private boolean redisbursement = false;

        public EasypayDisbursementWorker(Long contractId) throws Exception {
            this.contractId = contractId;
            apiType = cfg.getConfig("easypay.api.type");
            this.contractDetail = getContractDetail();
            this.cashflowId = contractDetail.getLong("cashflow_id");
        }

        public void run() {
            if (contractDetail.getInt("status_id") != DataUtils.STATUS_CONTRACT_SIGNED) {
                log.warn("Failed! Contract "+contractId+" is not eligible to disburse!");
                return;
            }
            log.info("Starting disburse #"+contractId);
            try {
                disburse(contractDetail);
            } catch (Exception e) {
                log.error("Error while disbursement: "+e,e);
            }
        }

        private JSONObject getContractDetail() throws Exception {
            try {
                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("contractId", contractId);
                params.addValue("typeId", DataUtils.TYPE_CASHFLOW_OUTFLOW);
                params.addValue("categoryId", DataUtils.CATEGORY_CASHFLOW_DISBURSEMENT);
                params.addValue("cashflowStatusId", DataUtils.STATUS_CASHFLOW_PENDING);
                params.addValue("contractStatusId", DataUtils.STATUS_CONTRACT_SIGNED);

                String sql = " SELECT c.*,f.id as cashflow_id, trx_date  " +
                        " FROM _contract c JOIN _cashflow f ON (c.contract_id=f.contract_id) WHERE 1 " +
                        " AND c.contract_id=:contractId AND c.status_id=:contractStatusId AND f.type_id=:typeId AND f.category_id=:categoryId AND f.status_id=:cashflowStatusId ";

                Map<String, Object> qr = dataUtils.getJT().queryForMap(sql, params);
                JSONObject jsonResult = new JSONObject(qr);
                return jsonResult;
            } catch (EmptyResultDataAccessException e) {
                log.error("Error retrieving contract #"+contractId+", "+e);
                throw new Exception("Invalid contract #"+contractId);
            } catch (Exception e) {
                log.error("Error unknown #"+contractId+", "+e);
                throw new Exception("Invalid contract #"+contractId);
            }
        }

        private void checkIfInProcess() throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);
            params.addValue("cashflowId", cashflowId);
            params.addValue("method", METHOD_DISBURSEMENT);

            String sql = " SELECT COUNT(*) FROM easypay_api_log WHERE cashflow_id=:cashflowId AND in_process=1 AND method=:method ";
            Integer found = dataUtils.getJT().queryForObject(sql, params, Integer.class);
            if (found>0) throw new Exception("Another disbursement process is on progress!");
        }

        private boolean checkPrevRequest() throws Exception {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("contractId", contractId);
            params.addValue("cashflowId", cashflowId);
            params.addValue("method", METHOD_DISBURSEMENT);

            String sql = " SELECT * FROM easypay_api_log WHERE cashflow_id=:cashflowId AND in_process=0  AND method=:method" +
                    " ORDER BY ins_on DESC LIMIT 0,1 ";

            List<Map<String,Object>> rows = dataUtils.getJT().queryForList(sql, params);
            if (rows.size() <= 0) {
                log.info("No similar transaction previously!");
                return false;
            }

            JSONObject prevReq = new JSONObject(rows.get(0));
            Long logId = prevReq.getLong("id");
            JSONObject jsonResp = easypayUtils.inquiryStatus(logId);

            if (jsonResp.getString("STATUS").equals("SUCCESS")) {
                // activate the contract
                log.info("Previous request was success nothing to do with further request!");
                return true;
            } else {
                log.info("Previous request was fail redisburse is allowed!");
                return false;
            }
        }

        public void disburse(JSONObject contractDetail) throws Exception {

            Long logId = null;

            if (checkPrevRequest()) return;

            try {
                final String secretKey = cfg.getConfig("easypay.api.secret.key." + apiType);
                final String trxTime = DateTime.now().toString("yyyy-MM-dd HH:mm:ss");
                final String easypayCode = cfg.getConfig("easypay.api.code");

                JSONObject jsonReq = new JSONObject();
                jsonReq.put("METHOD", METHOD_DISBURSEMENT);
                jsonReq.put("CODE", easypayCode);
                jsonReq.put("TRANSACTIONTIME", trxTime);

                String signatureRaw = METHOD_DISBURSEMENT + easypayCode + trxTime + secretKey;
                log.debug("Signature (Raw): " + signatureRaw);
                String signature = DigestUtils.sha256Hex(signatureRaw);
                log.info("Sinature: " + signature);
                jsonReq.put("SIGNATURE", signature);

                String trxId = UUID.randomUUID().toString().toUpperCase();
                JSONObject jsonTrx = new JSONObject();
                jsonTrx.put("TRANSACTIONID", trxId);
                jsonTrx.put("ID", contractDetail.getString("easypay_id"));
                jsonTrx.put("AMOUNT", contractDetail.get("amount_approved"));

                JSONArray jsonTrxs = new JSONArray();
                jsonTrxs.put(jsonTrx);

                jsonReq.put("TRANSACTIONS", jsonTrxs);

                // add to log
                logId = easypayUtils.insertToLog(contractDetail, jsonReq);


                log.info("JSON Request: " + jsonReq.toString(4));


                JSONObject jsonToken = easypayUtils.getValidToken();
                log.info("Valid token: " + jsonToken.toString());

                String url = cfg.getConfig("easypay.api.trx.url." + apiType) + "/v1/credits/transaction";
                String authHeader = "Bearer " + jsonToken.getString("access_token");
                log.debug("Access URL: " + url + ", token: " + authHeader);

                JSONObject jsonResp = easypayUtils.postJson(url, jsonReq, authHeader);
                log.info("Received response: " + jsonResp.toString(4));

                JSONObject jsonRespTrx = jsonResp.getJSONArray("TRANSACTIONS").getJSONObject(0);

                String respReceiptId = (jsonRespTrx.has("RECEIPTID") && !jsonRespTrx.isNull("RECEIPTID")) ? "" + jsonRespTrx.get("RECEIPTID") : null;
                String respStatus = (jsonRespTrx.has("STATUS")) ? "" + jsonRespTrx.get("STATUS") : null;
                Long respValue = (jsonRespTrx.has("VALUE")) ? jsonRespTrx.getLong("VALUE") : null;
                String respError = (jsonRespTrx.has("ERROR_MESSAGE")) ? "" + jsonRespTrx.get("ERROR_MESSAGE") : null;

                easypayUtils.updateLog(logId, respReceiptId, respStatus, respValue, respError);

                if (respStatus.equals("SUCCESS")) {
                    JSONObject activationReq = new JSONObject();
                    activationReq.put("contractId", contractId);
                    activationReq.put("activateBy", 1);
                    log.info("Send activation request: "+activationReq.toString());
                    messageSender.send(QUEUE_ACTIVATE_CONTRACT, activationReq);
                }


            }
            catch (Exception e) {
                log.error("Error while requesting disbursement: "+e,e);
                if (logId !=null) {
                    easypayUtils.updateLog(logId, null, "ERROR", 0, e.getMessage());
                    Utils.sendError(context, "Disbursement error, contract #: "+contractId, e);
                }
            }
        }
    }


}
